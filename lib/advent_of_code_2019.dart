import 'src/days/Day.dart';
import 'src/days/DayBuilder.dart';

void run() {
  Day day = DayBuilder()
      .build(13);
  print(day.part1());
  print(day.part2());
}
